Humanfork: Spring-Data-JPA Extension
====================================

A util project that provides nullaware support for Spring-Data-JPA Repositories.
The nullaware-framework allows to define what to do when a repository query has an empty result.

Background / UseCase - what is this framework for
-------------------------------------------------

For repository methods with an single return object/entity, but not an collection, it is without this framework common to return `null` when the query return no result.
But often the client code requires/expect that there is an returned entity and therefore the first thing the client code does, it to check that the result is not null.

```
Customer customer = customerRepository.getKey(String key);
if (customer == null) {
   throw new NotFoundRuntimeException("No customer with key=" + key);
}
```

This short code snippet is needed everywhere, where a repository query with a single return values is used and it is expected that the query return an object but not null.  

Of course it depends on the use case: whether it is expected that the query always return an object but no object  (`null`) is an defect that should result in an exception, or it is the expected behavior that the repository can return null to indicate that something is not found (In this case think about to use an Optional).

This framework is for the fist of this both use cases: It allows to specify that a repository method always return an object that was queried for, or do an alternative handling when no object was found by the query, for example: throw an exception.

Usage
-----

Chose the strategy (implements `de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction` you want to been executed when the query result is empty and the matching factory (implements`de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory`
_Have a look at package documentation `de.humanfork.spring.data.jpa.nullaware.controll` for an summary of the provides strategies_ )

There are two ways to create the Spring Data JPA Repository with the empty result controlling strategy:

- Create the Spring Data JPA Repository with `de.humanfork.spring.data.jpa.nullaware.NullawareRepositoryProxyPostProcessor` as `org.springframework.data.repository.core.support.RepositoryProxyPostProcessor RepositoryProxyPostProcessor`. The `de.humanfork.spring.data.jpa.nullaware.NullawareRepositoryProxyPostProcessor NullawareRepositoryProxyPostProcessor` takes your selected `de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory` as constructor parameter. or
- Use the `de.humanfork.spring.data.jpa.nullaware.NullawareJpaRepositoryFactoryBean` as  `org.springframework.data.jpa.repository.support.JpaRepositoryFactory` to create the Spring Data JPA Repository. The `de.humanfork.spring.data.jpa.nullaware.NullawareJpaRepositoryFactoryBean` takes your selected `de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory` as constructor parameter.

Unfortunately Spring Data JPA requires a class but not a bean for `factory-class`. Therefore it is first required to create an subclass of `de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory` with an constructor (Since Spring Data JPA 1.11) with a single `Class<? extends T> repositoryInterface` argument.

Then one can use this class as `factory-class`
```
public class NullawareNameControlledJpaRepositoryFactoryBean<T extends JpaRepository<Object, Serializable>>
                                                            extends NullawareJpaRepositoryFactoryBean<T> {

   public NullawareNameControlledJpaRepositoryFactoryBean(final Class<? extends T> repositoryInterface) {
      super(repositoryInterface, new NameControlledNullResultActionFactory(NotFoundRuntimeException.class));
   }
}
```

```
<jpa:repositories base-package="com.example"
     factory-class="com.example.NullawareNameControlledJpaRepositoryFactoryBean">
```

@see <a href="http://docs.spring.io/spring-data/jpa/docs/current/reference/html/">2.6.2. Adding custom behavior to all repositories</a>

Maven
-----
Available from [Maven Central](https://search.maven.org/search?q=g:de.humanfork.hibernateextension)

<groupId>de.humanfork.springdatajpaextension</groupId>
	<artifactId>spring-data-jpa-extension</artifactId>

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.humanfork.springdatajpaextension/>spring-data-jpa-extension/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.humanfork.springdatajpaextension/>spring-data-jpa-extension)

```
<dependency>
    <groupId>de.humanfork.springdatajpaextension</groupId>
    <artifactId>spring-data-jpa-extension</artifactId>
    <version>0.4.2</version> <!-- or newer -->
</dependency>
```

Website
-------
- https://www.humanfork.de/projects/spring-data-jpa-extension/ (for releases)
- https://humanfork.gitlab.io/spring-data-jpa-extension/index.html (for snapshots)
