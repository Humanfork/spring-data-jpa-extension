/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;
import org.junit.Before;
import org.junit.Test;

import de.humanfork.spring.data.jpa.nullaware.controll.NullResultMessage;
import de.humanfork.spring.data.jpa.nullaware.controll.ThrowsControlledNullResultAction;
import de.humanfork.spring.data.jpa.nullaware.controll.helper.MethodInvocationMock;

/**
 * @author Ralph Engelmann
 */
public class ThrowsControlledNullResultActionTest {
    
 
    @NullResultMessage("There is something wrong with >{1}<")
    public Object getObjectByName(String name) throws RuntimeException {
        return null;
    }

    private Method queryMethod;

    private MethodInvocation invocation;

    @Before
    public void before() throws SecurityException, NoSuchMethodException {        
        this.queryMethod = ThrowsControlledNullResultActionTest.class.getMethod("getObjectByName", String.class);
        this.invocation = new MethodInvocationMock(new Object[]{"test"}, null, null, queryMethod);        
    }
    
    @Test
    public void testCreateException() {       
        /* Class under test */
        ThrowsControlledNullResultAction action = new ThrowsControlledNullResultAction(queryMethod);
        
        /* call method under test */
        RuntimeException result = action.createException(invocation);
        
        assertEquals(RuntimeException.class, result.getClass());
        assertEquals("There is something wrong with >test<", result.getMessage());
    }

}
