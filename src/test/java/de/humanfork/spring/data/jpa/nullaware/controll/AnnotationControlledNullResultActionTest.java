/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;
import org.junit.Before;
import org.junit.Test;

import de.humanfork.spring.data.jpa.nullaware.controll.AnnotationControlledNullResultAction;
import de.humanfork.spring.data.jpa.nullaware.controll.NullResultMessage;
import de.humanfork.spring.data.jpa.nullaware.controll.NullResultThrow;
import de.humanfork.spring.data.jpa.nullaware.controll.helper.MethodInvocationMock;

/**
 * @author Ralph Engelmann
 */
public class AnnotationControlledNullResultActionTest {

    @NullResultThrow(IllegalArgumentException.class)
    @NullResultMessage("you should select a better name than >{1}< next time")
    public Object getObjectByName(String name) {
        return null;
    }    

    private Method queryMethod;

    private MethodInvocation invocation;

    @Before
    public void before() throws SecurityException, NoSuchMethodException {
        this.queryMethod = AnnotationControlledNullResultActionTest.class.getMethod("getObjectByName", String.class);
        this.invocation = new MethodInvocationMock(new Object[] { "test" }, null, null, queryMethod);
    }

    @Test
    public void testIsActive() {

        /* class under test */
        AnnotationControlledNullResultAction action = new AnnotationControlledNullResultAction(queryMethod);

        /* call method under test */
        boolean result = action.isActive();

        assertTrue(result);
    }

    @Test
    public void testCreateException() {

        /* class under test */
        AnnotationControlledNullResultAction action = new AnnotationControlledNullResultAction(queryMethod);

        /* call method under test */
        RuntimeException result = action.createException(invocation);

        assertEquals(IllegalArgumentException.class, result.getClass());
        assertEquals("you should select a better name than >test< next time", result.getMessage());
    }

}
