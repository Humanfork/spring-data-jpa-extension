/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll.helper;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;

/** Mock implementation of {@link MethodInvocation} object. */
public class MethodInvocationMock implements MethodInvocation {

    private Object[] arguments;

    private Object thisObject;

    private AccessibleObject staticPart;

    private Method method;
    
    public MethodInvocationMock(Object[] arguments, Object thisObject, AccessibleObject staticPart, Method method) {
        this.arguments = arguments;
        this.thisObject = thisObject;
        this.staticPart = staticPart;
        this.method = method;
    }

    @Override
    public Object[] getArguments() {
        return this.arguments;
    }

    @Override
    public Object proceed() throws Throwable {
        throw new RuntimeException("not implemented");
    }

    @Override
    public Object getThis() {
        return this.thisObject;
    }

    @Override
    public AccessibleObject getStaticPart() {
        return this.staticPart;
    }

    @Override
    public Method getMethod() {
        return this.method;
    }

}
