/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll.helper;

import java.lang.annotation.Annotation;

import de.humanfork.spring.data.jpa.nullaware.controll.NullResultThrow;

/**
 * Mock for {@link NullResultThrow} annotation.
 * @author Ralph Engelmann
 */
public class NullResultThrowMock implements NullResultThrow {

    private Class<? extends RuntimeException> value;

    
    public NullResultThrowMock(Class<? extends RuntimeException> value) {
        super();
        this.value = value;
    }

    public Class<? extends RuntimeException> value() {
        return value;
    }
    

    public Class<? extends Annotation> annotationType() {
       return NullResultThrow.class;
    }

}
