/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import org.aopalliance.intercept.MethodInvocation;

/**
 * Handle empty results by doing noting and return a simple {@code null}. This
 * class is immutable.
 * 
 * @author Ralph Engelmann
 */
public final class NullReturningNullResultAction implements NullResultAction {

    /** The one and only instance. */
    public static final NullReturningNullResultAction INSTANCE = new NullReturningNullResultAction();


    /** Use the {@link NullReturningNullResultAction#INSTANCE} variable instead of this. */
    private NullReturningNullResultAction() {
        super();
    }

    /**
     * Return {@code null}
     * @return null
     */
    public Object handleEmptyResult(MethodInvocation methodInvocation) {
        return null;
    }
}
