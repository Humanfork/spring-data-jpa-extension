/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import org.aopalliance.intercept.MethodInvocation;

/**
 * Instance of this interface describe what to do if an query has an empty result.
 * 
 * @author Ralph Engelmann
 */
public interface NullResultAction {

    /**
     * This method will be invoked if a query has an empty result. The
     * implementation of this method defines how to handle this situation. It
     * can for example return {@code null} or rise an exception.
     * 
     * @param invocation the method invocation
     * @return The return value for the query in case of an empty result query.
     */
    Object handleEmptyResult(MethodInvocation invocation);
}
