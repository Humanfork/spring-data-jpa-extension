/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.support.RepositoryProxyPostProcessor;

import de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory;

/**
 * Add an {@link NullawareMethodAdvice} with the {@link NullawareRepositoryProxyPostProcessor} to the Repository.
 * @author Ralph Engelmann
 */
public class NullawareRepositoryProxyPostProcessor implements RepositoryProxyPostProcessor {

    /**  The null result action factory. */
    private final NullResultActionFactory nullResultActionFactory;

    /**
     * Instantiates a new nullaware repository proxy post processor.
     *
     * @param nullResultActionFactory the null result action factory
     */
    public NullawareRepositoryProxyPostProcessor(NullResultActionFactory nullResultActionFactory) {
        assert nullResultActionFactory != null;
        
        this.nullResultActionFactory = nullResultActionFactory;
    }

    @Override
    public void postProcess(ProxyFactory factory, RepositoryInformation repositoryInformation) {
        factory.addAdvice(new NullawareMethodAdvice(nullResultActionFactory));
    }
}
