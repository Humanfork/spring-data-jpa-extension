/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Specify the messages that put into the exception that is thrown in case of an empty result.
 * The message(s) specified have to be <strong>valid {@link java.text.MessageFormat}
 * patterns</strong>!
 * 
 * @author Ralph Engelmann
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface NullResultMessage {

    /**
     * The messages. Every array item is associated with one exception
     * constructor parameter. Each string have to be a <strong>valid
     * {@link java.text.MessageFormat} pattern</strong>.
     * 
     * In order to be consistent with the way how parameter are used in queries,
     * the first method parameter is bound the the message pattern parameter <code>{1}</code>.
     * 
     * @return null message
     */
    String[] value();
}
