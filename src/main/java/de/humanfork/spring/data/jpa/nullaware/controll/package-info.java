/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * Provides different ways of handling an empty query result.
 * 
 * <p>
 * Each way is responsible to detect whenever a repository method is allowed to return null or not,
 * and what to do when the query result is empty but null is not an allowed return value.
 * </p>
 * 
 * <p>
 * The SPI interface is {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction}.
 * An implementation of this interface is responsible to decide what to do, when the query result is empty.
 * </p>
 * 
 * <p>
 * The provided implementations are:
 * </p>
 * <ul>
 *  <li>
 *      {@link de.humanfork.spring.data.jpa.nullaware.controll.NullReturningNullResultAction} -
 *      just return {@code null}
 *  </li>
 *  <li>
 *      {@link de.humanfork.spring.data.jpa.nullaware.controll.ThrowsControlledNullResultAction} - 
 *      when the query result is empty and the method has a {@code throws} clause, then this exception is thrown.
 *  </li>
 *  <li>
 *      {@link de.humanfork.spring.data.jpa.nullaware.controll.AnnotationControlledNullResultAction} - 
 *      when the query result is empty and the method has a
 *      {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultThrow} annotation, then this exception is
 *      thrown.
 *  </li>
 *  <li>
 *      {@link de.humanfork.spring.data.jpa.nullaware.controll.NameControlledNullResultAction} -
 *      when the query result is empty and repository method name starts with {@code get} then an configured
 *      exception is thrown.
 *  </li>
 * </ul>
 * <p>
 * ({@link de.humanfork.spring.data.jpa.nullaware.controll.ExceptionThrowingNullResultAction} is a abstract  class that
 * can been uses for implementing custom {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction})s.
 * </p>
 * 
 * <p>All provided implementations that throw an exception instead of returning null are feasible to have a customized
 * exception message, specified by the {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultMessage}
 * annotation. This feature is implemented by the abstract class
 * {@link de.humanfork.spring.data.jpa.nullaware.controll.ExceptionThrowingNullResultAction}.
 * (see this class for more details)
 * </p>
 * 
 * <p>
 * Whenever the nullaware-framework needs an {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction},
 * it creates an instance of this interface by using the 
 * {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory}. Therefore one need a 
 * such a factory implementation for each null result action implementation. (The naming pattern used for the provided
 * null result actions is the name of the action with postfix "factory".)
 * </p>
 */
package de.humanfork.spring.data.jpa.nullaware.controll;