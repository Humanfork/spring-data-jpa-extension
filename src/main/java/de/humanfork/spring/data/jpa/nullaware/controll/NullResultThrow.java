/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Indicates that the single entity query method annotated with this annotation
 * should throw an exception instead return null.
 * 
 * @author Ralph Engelmann
 * @see de.humanfork.spring.data.jpa.nullaware.controll.AnnotationControlledNullResultAction
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface NullResultThrow {

    /**
     * What kind of Runtime exception should be thrown. Default is
     * {@link java.lang.RuntimeException}.
     * 
     * @return Exception for null
     */
    Class<? extends RuntimeException> value() default RuntimeException.class;
}
