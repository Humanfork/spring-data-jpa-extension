/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.reflect.Method;

/**
 * Handle No Result queries be throwing an exception, the concrete Exception is
 * describe via Annotation.
 * 
 * @author Ralph Engelmann
 */
public class AnnotationControlledNullResultAction extends ExceptionThrowingNullResultAction implements NullResultAction {

    /**
     * Instantiates a new annotation controlled no result action.
     * 
     * @param method the query method guarded by this action.
     */
    public AnnotationControlledNullResultAction(final Method method) {

        super(method);
    }

    @Override
    protected boolean isActive() {
        return findThrowAnnotation() != null;
    }

    /**
     * Search for the {@link NullResultThrow} annotation on the query method. If
     * the method is not annotated with this annotation, the return value is
     * {@code null}.
     * 
     * @return the {@link NullResultAction} Annotation or {@code null}.
     */
    public NullResultThrow findThrowAnnotation() {
        return getMethod().getAnnotation(NullResultThrow.class);
    }

    @Override
    protected Class<? extends RuntimeException> determineExceptionClass() {

        Class<? extends RuntimeException> exceptionClass = findThrowAnnotation().value();

        if (exceptionClass == null) {
            throw new RuntimeException("the EmptyResultThrow annotation have to define a runtime exception class");
        }

        return exceptionClass;
    }

}
