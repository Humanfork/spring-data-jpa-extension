/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction;
import de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory;

/**
 * Method Interceptor advice, that invoke the {@link NullResultAction#handleEmptyResult(MethodInvocation)}
 * method (provided by the {@link NullResultActionFactory}) when the adviced method returns null.
 */
class NullawareMethodAdvice implements MethodInterceptor {

    /**  The null result action factory. */
    private final NullResultActionFactory nullResultActionFactory;

    /**
     * Instantiates a new nullaware method advice.
     *
     * @param nullResultActionFactory the null result action factory
     */
    NullawareMethodAdvice(NullResultActionFactory nullResultActionFactory) {
        assert nullResultActionFactory != null;

        this.nullResultActionFactory = nullResultActionFactory;
    }

    /**
     * Create a {@link NullResultAction} and let it handle the empty result, when the method result is null.
     *
     * @param invocation the invocation
     * @return the object
     * @throws Throwable the throwable
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        Object result = invocation.proceed();
        if (result == null) {

            //TODO improvement caching of NullResultAction s for methods.
            return nullResultActionFactory.createNullResultAction(invocation.getMethod()).handleEmptyResult(invocation);
        } else {
            return result;
        }
    }

}
