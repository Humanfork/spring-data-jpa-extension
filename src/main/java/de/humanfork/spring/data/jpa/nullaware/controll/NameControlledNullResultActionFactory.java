/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.reflect.Method;

/**
 * A factory for creating NameControlledNullResultAction objects.
 */
public class NameControlledNullResultActionFactory implements NullResultActionFactory {

    /** The exception that is thrown, when no exception is defined by an throws clause. */
    private final Class<? extends RuntimeException> defaultExceptionClass;

    /**
     * Instantiates a new name controlled empty result action.
     *
     * @param defaultExceptionClass that is thrown, when no exception is defined by an throws clause.
     */
    public NameControlledNullResultActionFactory(final Class<? extends RuntimeException> defaultExceptionClass) {
        this.defaultExceptionClass = defaultExceptionClass;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.synyx.hades.dao.orm.emptyfactory.NullResultActionFactory#createNullResultAction(java.lang.reflect.Method)
     */
    @Override
    public NullResultAction createNullResultAction(final Method method) {
        return new NameControlledNullResultAction(method, defaultExceptionClass);
    }

}
