/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
/**
 * The nullaware-framework allows to define what to do when a repository query has an empty result.
 * 
 * 
 * <h1>Background / UseCase - what is this framework for</h1>
 * <p>
 * For repository methods with an single return object/entity, but not an collection, it is without this
 * framework common to return <code>null</code> when the query return no result. But often the client code
 * requires/expect that there is an returned entity and therefore the first thing the client code does,
 * it to check that the result is not null.
 * </p>
 * <pre>
 *    Customer customer = customerRepository.getKey(String key);
 *    if (customer == null) {
 *       throw new NotFoundRuntimeException("No customer with key=" + key);
 *    }
 * </pre>
 * <p>
 * This short code snippet is needed everywhere, where a repository query with a single return values
 * is used and it is expected that the query return an object but not null.  
 * </p>
 * <p>
 * Of course it depends on the use case: whether it is expected that the query always return an object but no object
 * (<code>null</code>) is an defect that should result in an exception, or it is the expected behavior that the
 * repository can return null to indicate that something is not found (In this case think about to use an Optional).
 * </p>
 * 
 * <p>
 * This framework is for the fist of this both use cases: It allows to specify that a repository method always 
 * return an object that was queried for, or do an alternative handling when no object was found by the query,
 * for example: throw an exception.  
 * </p>
 * 
 * <h1>Usage</h1>
 * <p>
 * Chose the strategy (implements {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction}) you want to
 * been executed when the query result is empty and the matching factory (implements
 * {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory}). 
 * <i>Hav a look at package documentation {@link de.humanfork.spring.data.jpa.nullaware.controll} for an summary of the
 * provides strategies)</i> 
 * </p>
 * 
 * <p>
 * There are two ways to create the Spring Data JPA Repository with the empty result controlling strategy:
 * </p>
 * <ul>
 *    <li>
 *       Create the Spring Data JPA Repository with
 *       {@link de.humanfork.spring.data.jpa.nullaware.NullawareRepositoryProxyPostProcessor} as
 *       {@link org.springframework.data.repository.core.support.RepositoryProxyPostProcessor RepositoryProxyPostProcessor}. 
 *       
 *       The {@link de.humanfork.spring.data.jpa.nullaware.NullawareRepositoryProxyPostProcessor NullawareRepositoryProxyPostProcessor}
 *       takes your selected {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory} as
 *       constructor parameter.
 *       or
 *    </li>
 *    <li>
 *       <p>
 *       Use the {@link de.humanfork.spring.data.jpa.nullaware.NullawareJpaRepositoryFactoryBean} as 
 *       {@link org.springframework.data.jpa.repository.support.JpaRepositoryFactory} to create the Spring Data JPA Repository.
 *       The {@link de.humanfork.spring.data.jpa.nullaware.NullawareJpaRepositoryFactoryBean} takes your selected
 *       {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory} as constructor parameter.
 *       </p>
 *       <p>
 *       Unfortunately Spring Data JPA requires a class but not a bean for <code>factory-class</code>. Therefore it is first required
 *       to create an subclass of {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory} with an
 *       constructor (Since Spring Data JPA 1.11) wiht a single {@code Class<? extends T> repositoryInterface} argument.
 *       Then one can use this class as <code>factory-class</code>
 *       </p>
 *       
 *       example:
 *       <pre>
 *       public class NullawareNameControlledJpaRepositoryFactoryBean&lt;T extends JpaRepository&lt;Object, Serializable&gt;&gt;
 *                                                                   extends NullawareJpaRepositoryFactoryBean&lt;T {
 *
 *          public NullawareNameControlledJpaRepositoryFactoryBean(final Class&lt;? extends T&gt; repositoryInterface) {
 *             super(repositoryInterface, new NameControlledNullResultActionFactory(NotFoundRuntimeException.class));
 *          }
 *       }
 *       </pre> 
 *       <pre>
 *          &lt;jpa:repositories base-package="com.example"
 *               factory-class="com.example.NullawareNameControlledJpaRepositoryFactoryBean"/&gt;
 *       </pre>
 *    </li>
 * </ul>
 * 
 * @see <a href="http://docs.spring.io/spring-data/jpa/docs/current/reference/html/">2.6.2. Adding custom behavior to all repositories</a>
 */
package de.humanfork.spring.data.jpa.nullaware;