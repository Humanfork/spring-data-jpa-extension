/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.reflect.Method;

/**
 * Null Result Action that can be triggered by name or by naming convention or explicit throws annotation.
 * The naming convention is, that if the method name starts with {@code get} then an {@link RuntimeException}
 * is thrown. 
 * 
 * <p>
 * The thrown exception is configured by {@link #defaultExceptionClass}.
 * </p> 
 * 
 * @author Ralph Engelmann
 */
public class NameControlledNullResultAction extends ExceptionThrowingNullResultAction implements NullResultAction {

    /** The exception that is thrown, when no exception is defined by an throws clause. */
    private final Class<? extends RuntimeException> defaultExceptionClass;

    /**
     * Instantiates a new name controlled empty result action.
     *
     * @param method the query method guarded by this action.
     * @param defaultExceptionClass that is thrown, when no exception is defined by an throws clause.
     */
    public NameControlledNullResultAction(final Method method,
            final Class<? extends RuntimeException> defaultExceptionClass) {
        super(method);
        this.defaultExceptionClass = defaultExceptionClass;
    }

    @Override
    protected boolean isActive() {
        return getMethod().getName().startsWith("get") || (getMethod().getExceptionTypes().length > 0);

    }

    @Override
    protected Class<? extends RuntimeException> determineExceptionClass() {

        Class<? extends RuntimeException> throwsException = determineExceptionClassByThrows();
        if (throwsException != null) {
            return throwsException;
        } else {
            return defaultExceptionClass;
        }
    }

    /**
     * Determine the exception class by the throw clause of the method.
     * @return the exception class or null
     * @throws RuntimeException if there is more than one exception declared
     * or the declared exception in not an runtime exception.
     */
    protected Class<? extends RuntimeException> determineExceptionClassByThrows() throws RuntimeException {

        Class<?>[] declaredExceptions = getMethod().getExceptionTypes();

        if (declaredExceptions.length == 0) {
            return null;
        }
        if (declaredExceptions.length > 1) {
            throw new RuntimeException("no unique exception class in throws declaration - method="
                    + getMethod().getName());
        }

        /*
         * cast is save, because every declared exception is a Throwable or a subclass
         */
        @SuppressWarnings("unchecked")
        Class<? extends Throwable> throwableClass = (Class<? extends Throwable>) declaredExceptions[0];

        if (RuntimeException.class.isAssignableFrom(throwableClass)) {
            /* this cast is save, because of the check */
            @SuppressWarnings("unchecked")
            Class<? extends RuntimeException> runtimeExceptionClass = (Class<? extends RuntimeException>) throwableClass;
            return runtimeExceptionClass;
        } else {
            throw new RuntimeException("only runtime exceptions are alowed - method=" + getMethod().getName());
        }
    }

}
