/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.reflect.Method;

/**
 * Handle empty results of queries be throwing an exception, the concrete
 * exception type is is defined by the throw clause of the query method.
 * <ul>
 * <li><strong>Only Runtime Exceptions are supported thrown!</strong></li>
 * <li><strong>Only one declared exception per method!</strong></li>
 * </ul>
 * 
 * Example: 
 * <pre>
 *      Car getByManufacturer(Manufacturer m) throws IllegalArgumentException
 * </pre> 
 * 
 * <p>
 * The Message of the exception an be specified with an
 * {@link NullResultMessage} annotation. Example:
 * </p> 
 * <pre>
 *      &#64;NullResultMessage("car not found by manufacturer {1}")
 *      Car getByManufacturer(Manufacturer m) throws IllegalArgumentException
 * </pre>
 * 
 * @author Ralph Engelmann
 */
public class ThrowsControlledNullResultAction extends ExceptionThrowingNullResultAction implements NullResultAction {

    /**
     * Instantiates a new annotation controlled no result action.
     * 
     * @param method the query method guarded by this action.
     */
    public ThrowsControlledNullResultAction(final Method method) {
        super(method);
    }

    /**
     * This {@code EmptyResultAction} is active if the method is annotated with
     * an throw clause.
     */
    @Override
    protected boolean isActive() {
        return (this.getMethod().getExceptionTypes().length > 0);
    }

    @Override
    protected Class<? extends RuntimeException> determineExceptionClass() {
        Class<?>[] declaredExceptions = this.getMethod().getExceptionTypes();

        if (declaredExceptions.length == 0) {
            throw new RuntimeException("no throws declaration - method=" + this.getMethod().getName());
        }
        if (declaredExceptions.length > 1) {
            throw new RuntimeException("no unique exception class in throws declaration - method="
                    + this.getMethod().getName());
        }

        /*
         * cast is save, because every declared exception is a Throwable or a subclass
         */
        @SuppressWarnings("unchecked")
        Class<? extends Throwable> throwableClass = (Class<? extends Throwable>) declaredExceptions[0];

        if (RuntimeException.class.isAssignableFrom(throwableClass)) {
            /* this cast is save, because of the check */
            @SuppressWarnings("unchecked")
            Class<? extends RuntimeException> runtimeExceptionClass = (Class<? extends RuntimeException>) throwableClass;
            return runtimeExceptionClass;
        } else {
            throw new RuntimeException("only runtime exceptions are alowed - method=" + getMethod().getName());
        }
    }

}
