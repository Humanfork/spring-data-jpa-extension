/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware.controll;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.Assert;

/**
 * An Template class for {@link NullResultAction} implementations which handle
 * the empty result situation by rising an runtime exception.
 * <p>
 * This template pay attention to the {@link NullResultMessage} annotation. If
 * one exists and an exception is throw, the messages from the annotation will
 * be filled with the query method parameters. In order to be consistent with
 * the way how parameter are used in queries, the first method parameter is
 * bound the the message pattern parameter {1}. For more information look at
 * {@link NullResultMessage} and {@code ParameterBinder#bind(MessageFormat)}.
 * </p>
 * <p>
 * It is important the the used exception has a constructor with: exactly as many
 * {@link String} parameters as message Patterns defined by
 * {@link NullResultMessage#value()} annotation. 
 * </p>
 * 
 * <p>
 * These implementation requires all subclasses to implement two methods:
 * </p>
 * <ul>
 * <li>{@link ExceptionThrowingNullResultAction#isActive()}</li>
 * <li>{@link ExceptionThrowingNullResultAction#determineExceptionClass()}</li>
 * </ul>
 * The implemented algorithm for handling an invocation of
 * {@link NullResultAction#handleEmptyResult(MethodInvocation)} is:
 * <ol>
 * <li>The subclass implementation of
 * {@link ExceptionThrowingNullResultAction#isActive()} is invoked to determine
 * if an exception should be throw or not. If not (return false), then {@code null} is
 * returned, else the algorithm continues. (This method is intended to be used
 * to indicate whenever or not the subclass feels responsible to handle a null result
 * for the invoked repository method.)</li>
 * <li>The subclass implementation of
 * {@link ExceptionThrowingNullResultAction#determineExceptionClass()} is
 * invoked to determine what type of runtime exception should be thrown.</li>
 * <li>The {@link NullResultMessage} annotation will be inspected. If one
 * exists it messages patterns will be filled with the query method parameters.
 * Therefore the {@code ParameterBinder#bind(MessageFormat)} method is used.</li>
 * <li>A instance of the exception is created by invoking a constructor with
 * {@code n} String arguments. Where {@code n} is the number of message patterns
 * defined in {@link NullResultMessage} annotation. (or zero if there is not
 * annotation).</li>
 * </ol>
 * 
 * @see NullResultMessage
 *
 */
public abstract class ExceptionThrowingNullResultAction implements NullResultAction {

    /** The Query Method. */
    private final Method method;

    /**
     * Instantiates a new annotation controlled no result action.
     * 
     * @param method the query method guarded by this action.
     */    
    public ExceptionThrowingNullResultAction(final Method method) {
        Assert.notNull(method, "The argument `method` is required; it must not be null");

        this.method = method;
    }

    /**
     * Get the query method.
     * 
     * @return the query method.
     */
    protected Method getMethod() {
        return this.method;
    }

    @Override
    public Object handleEmptyResult(MethodInvocation invocation) {
        Assert.notNull(method, "The argument `invocation` is required; it must not be null");

        if (isActive()) {
            RuntimeException e = createException(invocation);
            throw e;
        } else {
            return null;
        }
    }

    /**
     * Check if this {@link NullResultAction} is enabled for its method (
     * {@link ExceptionThrowingNullResultAction#method}).
     * 
     * @return true if it is active.
     */
    protected abstract boolean isActive();

    /**
     * Create the exception which will be thrown because of an empty result.
     * 
     * @param methodInvocation the intercepted method invocation
     * @return the exception
     */
    protected RuntimeException createException(MethodInvocation methodInvocation) {

        Class<? extends RuntimeException> exceptionClass = determineExceptionClass();
        MessageFormat[] messagePatterns = determineMessagePatterns();
        Object[] stringArguments = createExceptionArguments(messagePatterns, methodInvocation);
        RuntimeException e = instanciateExceptionInstance(exceptionClass, stringArguments);
        return e;
    }

    /**
     * Determine the type of exception which is thrown because of an empty
     * result.
     * 
     * @return the exception type
     */
    protected abstract Class<? extends RuntimeException> determineExceptionClass();

    /**
     * Determine the message patters from the @link EmptyResultMessage}
     * annotation. If there is no annotation, then the result is an empty array.
     * 
     * @return the message patters
     */
    protected MessageFormat[] determineMessagePatterns() {

        MessageFormat[] messagePatterns;
        NullResultMessage messageAnnotation = findMessageAnnotation();
        if (messageAnnotation != null && messageAnnotation.value() != null) {
            messagePatterns = buildMessageFormats(messageAnnotation.value());
        } else {
            messagePatterns = new MessageFormat[0];
        }
        return messagePatterns;
    }

    /**
     * Create the message formatter objects for the pattern.
     * 
     * @param patterns the patterns
     * @return the message formatter
     */
    protected MessageFormat[] buildMessageFormats(String[] patterns) {

        int size = patterns.length;
        MessageFormat[] result = new MessageFormat[size];
        for (int i = 0; i < size; i++) {
            result[i] = new MessageFormat(patterns[i]);
        }
        return result;
    }

    /**
     * Search for the {@link NullResultMessage} annotation on the query method.
     * If the method is not annotated with this annotation, the return value is
     * {@code null}.
     * 
     * @return the {@link NullResultMessage} Annotation or {@code null}.
     */
    public NullResultMessage findMessageAnnotation() {

        return method.getAnnotation(NullResultMessage.class);
    }

    private Object[] createExceptionArguments(MessageFormat[] messagePatterns, MethodInvocation methodInvocation) {

        assert messagePatterns != null;
        assert methodInvocation != null;

        int numberOfPatterns = messagePatterns.length;

        Object[] arguments = new String[numberOfPatterns];
        for (int i = 0; i < numberOfPatterns; i++) {
            arguments[i] = bindExceptionMessage(messagePatterns[i], methodInvocation.getArguments());
        }

        return arguments;
    }
    
    
    
    
    /**
     * Bind the parameter to an message pattern.
     * In order to be consistent with
     * the way how parameter are used in queries, <strong>the first (method)
     * parameter is bound the the message pattern parameter number 1</strong>.
     * 
     * @param messagePattern the message pattern
     * @param methodArguments the method arguments
     * @return the string resulting from binding the parameters to the message
     *         pattern
     */
    public String bindExceptionMessage(final MessageFormat messagePattern, Object[] methodArguments) {

        int numberOfMethodParameters = methodArguments.length;

        Object[] arguments = new Object[numberOfMethodParameters + 1];
        arguments[0] = null;
        for (int i = 0; i < numberOfMethodParameters; i++) {
            arguments[i + 1] = methodArguments[i];
        }

        return messagePattern.format(arguments);
    }

    /**
     * Instantiate a new Runtime Exception according to the
     * {@link NullResultThrow} Annotation.
     * 
     * @return the exception
     */
    private RuntimeException instanciateExceptionInstance(Class<? extends RuntimeException> exceptionClazz,
            Object[] stringArguments) {

        assert exceptionClazz != null;
        assert stringArguments != null;

        try {
            Constructor<? extends RuntimeException> constructor = findConstructor(exceptionClazz,
                    stringArguments.length);
            return constructor.newInstance(stringArguments);

        } catch (IllegalArgumentException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        } catch (SecurityException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        } catch (InstantiationException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        } catch (IllegalAccessException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        } catch (InvocationTargetException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        } catch (NoSuchMethodException e1) {
            throw new RuntimeException("error while creating no result exception", e1);
        }
    }

    private Constructor<? extends RuntimeException> findConstructor(Class<? extends RuntimeException> exceptionClazz,
            int numberOfStringParameter) throws SecurityException, NoSuchMethodException {

        assert exceptionClazz != null;

        Class<?>[] parameterSignature = new Class<?>[numberOfStringParameter];
        Arrays.fill(parameterSignature, 0, numberOfStringParameter, String.class);

        return exceptionClazz.getConstructor(parameterSignature);
    }

}
