/*
 * Copyright 2014-2023 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package de.humanfork.spring.data.jpa.nullaware;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import de.humanfork.spring.data.jpa.nullaware.controll.NullResultActionFactory;

/**
 * A {@link JpaRepositoryFactoryBean} to create jpa repositories with and 
 * {@link de.humanfork.spring.data.jpa.nullaware.controll.NullResultAction} handling that is created
 * by the given {@link NullResultActionFactory}.
 * 
 * @author Ralph Engelmann
 */
public class NullawareJpaRepositoryFactoryBean<T extends JpaRepository<Object, Serializable>> extends
        JpaRepositoryFactoryBean<T, Object, Serializable> {
    
    /**  The null result action factory. */
    private final NullResultActionFactory nullResultActionFactory;

    /**
     * Instantiates a new nullaware jpa repository factory bean.
     *
     * @param repositoryInterface the repository interface
     * @param nullResultActionFactory the null result action factory
     */
    public NullawareJpaRepositoryFactoryBean(final Class<? extends T> repositoryInterface,
            final NullResultActionFactory nullResultActionFactory) {
        super(repositoryInterface);

        if(nullResultActionFactory == null) {
            throw new IllegalArgumentException("nullResultActionFactory must not be null");
        }
        
        this.nullResultActionFactory = nullResultActionFactory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.data.jpa.repository.support. GenericJpaRepositoryFactoryBean#getFactory()
     */
    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager em) {

        JpaRepositoryFactory jpaRepositoryFactory = new JpaRepositoryFactory(em);
        jpaRepositoryFactory
                .addRepositoryProxyPostProcessor(new NullawareRepositoryProxyPostProcessor(nullResultActionFactory));

        return jpaRepositoryFactory;
    }
}
